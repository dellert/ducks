import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import configureStore from './src/store';
import {Navigation} from './src/navigation';

const store = configureStore();

const App = () => {

  return (
    <Provider store={store}>
      <Navigation/>
    </Provider>
  );
};

export default App;
