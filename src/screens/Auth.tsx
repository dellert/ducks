import React, {useState} from 'react';
import {connect} from 'react-redux';
import {TouchableOpacity, Text, TextInput, View} from 'react-native';
import {operations} from '../modules/auth';
import navigator from '../navigation/navigator';

const Auth = ({isLoading, error, logIn}) => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const _handleLogIn = () => {
    logIn({login, password, type: 'android'})
      .then(res => {
        console.log('_handleLogIn result', res);

        if (!res.error) {
          navigator.navigate('Main');
        }
      });
  };

  return (
    <View style={{padding: 10}}>
      <Text>{isLoading ? 'Loading...' : null}</Text>

      {
        error ?
          error.map((item, i) => <Text key={i} style={{marginTop: 10, color: 'red'}}>{item}</Text>)
          : null
      }

      <TextInput style={{marginTop: 10, borderWidth: 1, borderColor: 'black'}}
                 onChangeText={setLogin} value={login}/>

      <TextInput style={{marginTop: 10, borderWidth: 1, borderColor: 'black'}}
                 onChangeText={setPassword} secureTextEntry/>

      <TouchableOpacity style={{marginTop: 10, backgroundColor: 'green', height: 30, alignItems: 'center'}}
                        onPress={() => _handleLogIn()}>
        <Text style={{color: 'white'}}>Enter</Text>
      </TouchableOpacity>
    </View>
  );
};

const mstp = state => ({
  isLoading: state.auth.logIn.loading,
  error: state.auth.logIn.error,
});

const mdtp = {
  logIn: data => operations.logIn(data),
};

export default connect(mstp, mdtp)(Auth);
