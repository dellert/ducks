import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import * as reducers from './modules';
import logger from 'redux-logger';

let middleware = applyMiddleware(thunkMiddleware, logger);

export default function configureStore() {
  return createStore(combineReducers(reducers), middleware);
}
