export interface User {
  accessToken?: string;
  id: number;
  fullName: string;
  email: string;
  organization?: string;
}
