import authReducer from './reducers';
import * as operations from './operations';
import * as actions from './actions';

export {
  operations,
  actions,
};

export default authReducer;
