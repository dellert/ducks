import API from '../../api/api';
import {LOG_IN, LOG_IN_ERROR, LOG_IN_SUCCESS} from './actions';
import {ApiResult} from '../../api/types';
import {UserFactory} from '../../factories/UserFactory';

export const logIn = (data: {}) => dispatch => {
  dispatch({type: LOG_IN});

  console.log('logIn data', data);

  return API.logIn(data)
    .then(response => response.data as ApiResult)
    .then(
      data => {

        if (data.success) {
          const user = UserFactory.create(data.data);

          console.log('user result', user);

          return dispatch({type: LOG_IN_SUCCESS, data: user});
        } else {
          return dispatch({type: LOG_IN_ERROR, error: data.errors});
        }
      },
      error => dispatch({type: LOG_IN_ERROR, error: [error.message || 'Unexpected Error']}),
    );
};
