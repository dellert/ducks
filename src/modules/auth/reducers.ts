import {LOG_IN, LOG_IN_ERROR, LOG_IN_SUCCESS} from './actions';
import {combineReducers} from 'redux';

const initialState = {
  loading: false,
  error: '',
};

export const logIn = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN: {
      return {
        ...state,
        loading: true,
        error: '',
      };
    }

    case LOG_IN_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: '',
      };
    }

    case LOG_IN_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    }

    default: {
      return state;
    }
  }
};

const reducer = combineReducers({
  logIn,
});

export default reducer;
