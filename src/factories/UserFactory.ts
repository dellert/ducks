import {User} from '../types/User';

export const UserFactory = {
    create: (data: object) => {
      return <User> {
        accessToken: data.accessToken,
        id: data.id,
        fullName: data.fullName,
        email: data.email,
        organization: data.organization,
      };
    },
};
