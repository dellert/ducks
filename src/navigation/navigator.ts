import * as React from 'react';
import {NavigationContainer, StackActions} from '@react-navigation/native';

export const navigationRef: NavigationContainer = React.createRef();

export interface NavigationParams {
  [key: string]: any;
}

const navigate = (routeName: string, params?: NavigationParams) => {
  console.log(navigationRef)
  navigationRef.current?.navigate(routeName, params);
};

const replace = (rootName, params?) => {
  navigationRef.current?.dispatch(StackActions.replace(rootName, params));
};

const push = (routeName: string, params?: NavigationParams) => {
  navigationRef.current?.dispatch(StackActions.push(routeName, params));
};

const pop = () => {
  navigationRef.current?.goBack();
};

export default {
  navigate,
  pop,
  push,
  replace,
};
