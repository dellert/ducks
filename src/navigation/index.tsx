import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {Auth, Main} from '../screens';

const Stack = createStackNavigator();

export const Navigation = ({navigationRef}: any) => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        headerMode="none"
        initialRouteName="Auth"
        screenOptions={{gestureEnabled: false, ...TransitionPresets.FadeFromBottomAndroid}}>
        <Stack.Screen name="Auth" options={{headerShown: false}} component={Auth}/>
        <Stack.Screen name="Main" options={{headerShown: false}} component={Main}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};
