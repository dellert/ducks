import axios, {AxiosError, AxiosRequestConfig} from 'axios';
import {BASE_URL, URL_AUTH, URL_LOGOUT} from './consts';
import {ApiResult} from './types';

axios.interceptors.request.use(
  (config) => {
    const newConfig: AxiosRequestConfig = {
      ...config,
      baseURL: BASE_URL,
      responseType: 'json',
      headers: {
        'Content-Type': 'application/json',
        'Access-Token': '',
      },
    };

    return newConfig;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  },
);

const API = {
  logIn: (data) => axios.post<ApiResult>(URL_AUTH, data),
  logOut: () => axios.post<ApiResult>(URL_LOGOUT),
};

export default API;
